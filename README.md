# Open Banking Simple Oauth

This project’s aim was to create an API that could check the validity of a requested token, and requesting for a refresh token through the Macquarie Bank API and store them in the database, while deleting the old invalid tokens.
The token, and the module itself, will be used as part of a larger application to be made by Empower Wealth, which is a financial advisor company based in Melbourne, Australia,  where the user can view and manage their financial status, such as their spending from a specific bank account. 


# Development

To develop, Node and NPM must be installed. Subsequently, all NPM packages must be installed.

`npm install`

Once all packages are installed and the required environment variables are set the development server can be started:

`npm start`

# Configuration

Configuration of server port, Macquarie base URL, Macquarie refresh token URL, client id, and client secret are stored in a module called config.js. This is done so that configuration data can be changed in a more modular way.


# Using the Macquarie API

A Macquarie developer portal account and application is required. Ensure the Macquarie application's "Callback URL" matches the callback route within the Express application.

To create an account register here: https://developer.macquariebank.io/.

The refresh token API works by first checking whether the user have a token stored in the database or not. If no token existed, the user will be propted to sign in using his/her Macquarie credentials. After following Macquarie's login flow, the Macquarie API will return a token object containing an access token, its expiry date, and a refresh token. This refresh token will be used for requesting a new access token upon access token expiration.

The API will periodically check the user's access token validity, if the token is still valid, no action is taken. If the access token is expired, the API will send a request to the Macquarie API for a new access token, referencing the older access token's refresh token in the request body. If the request is successful, the new access token will replace the old access token in the database.

## Using the Provided Postman Collection

A [Postman](https://www.getpostman.com/) collection has been provided: `Macquarie.postman_collection.json`. Once this is imported and the required variables are set it can be used.

Access authorization and variable configuartion via: **right clicking the collection name>Edit**.

- `access_token`: Sourced from the return of the Macquarie OAuth2 process: `{ "access_token": <token>, ... }`
- `client_id`: Must be provided initially
- `account_id`: This has been provided but may be changed

Additional Postman information:

- [Learn more about authorization](https://www.getpostman.com/docs/v6/postman/sending_api_requests/authorization)
- [Learn more about collection variables](https://www.getpostman.com/docs/v6/postman/environments_and_globals/variables)
