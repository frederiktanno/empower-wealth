require("dotenv").config();

const app = require("express")();
const port = 8080;

const bodyParser = require('body-parser');

app.use(bodyParser.json())

// Start server
app.listen(port, () =>
  console.log(`Server listening on port ${port}!`)
);

// Modules
const mongo = require("./mongoDb");




mongo.connect(function(err){
	const macquarie = require("./macquarie-api").router;

	app.use("/auth/macquarie", macquarie);
	//app.get("/checkToken",auth);
})





