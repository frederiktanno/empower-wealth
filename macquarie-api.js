/*
  This API handles user validation to the Macquarie Bank, specifically the initial log in to Macquarie
  and refreshing access to the Macquarie API
*/

const router = require("express").Router();
const qs = require("qs");
const axios = require("axios");
const config = require("./config");
let db;
var doAuth = false;

const client = require("./mongoDb");//a MongoDB instance, used for direct database access

db = client.getDb()

const oauthConfig = {
  authorizationURL: `${config.macquarie.MACQUARIE_BASE_URL}/user-interface/login`,
  tokenURL: `${config.macquarie.MACQUARIE_BASE_URL}/oauth2/token`,
  clientId: config.macquarie.MACQUARIE_CLIENT_ID,
  clientSecret: config.macquarie.MACQUARIE_CLIENT_SECRET
};

//here we store a list of account informations
var account={
  accountId: " ",
  accountName: " ",
  accountStatus: " ",
  productCategory: " ",
  productName: " "
}



//token information containing refresh token, access token expiry time, and scope
var tokenInfo={
    bankName:"macquarie",
    expiry:0,
    tokenType:"",
    refreshToken: "",
    accessToken : "",
    scope: "",
    clientId:"",
    refreshTokenExpiry:0
}

// This callback route must match the route specified in your Macquarie
// Application's "Callback URL". This includes matching the port
router.get("/callback", (req, res, next) => {
  const { code } = req.query;

  const data = qs.stringify({ grant_type: "authorization_code", code });

  // On token request Macquarie takes client_id and client_secret as headers.
  // This is non-standard and means packages like "oauth" will not work
  // ("passport-oauth2" package is reliant on the "oauth" package, so it too
  // will not work)
  const config = {
    headers: {
      client_id: oauthConfig.clientId,
      client_secret: oauthConfig.clientSecret
    }
  };

  axios
    .post(oauthConfig.tokenURL, data, config)
    .then(function(response){
      // Returns the current seconds since epoch, which is what we will be working with most
      const now = Math.round(new Date().getTime() / 1000);

      // retrieve the refresh token, scope, and access token expiry time, as well as refresh token expiry time
      tokenInfo.expiry = now + response.data.expires_in
      tokenInfo.tokenType = response.data.token_type
      tokenInfo.refreshToken = response.data.refresh_token
      tokenInfo.scope = response.data.scope
      tokenInfo.accessToken = response.data.access_token
      tokenInfo.refreshTokenExpiry = now + response.data.refresh_token_expires_in
      tokenInfo.clientId = oauthConfig.clientId

      res.json(response.data)

      db.collection("token").insertOne(tokenInfo,function(err,res){
        if(err)throw err;
        console.log("token data inserted");

      });

    })
    .catch(next);
});

function deleteToken(){
  var query = {clientId:oauthConfig.clientId}
    db
    .collection("token")
    .deleteOne(query,function(err,res){
      if(err) throw err
      console.log("old token deleted")
    })
}

//get the access token of a user.
function getToken(){
  db
  .collection("token")
  .find({clientId:oauthConfig.clientId}).toArray(function(err, result){
    if(err) throw err
    console.log(result[0])
    return result[0].accessToken
  })
}

//replace the existing token in the database with the new access token
function replaceToken(newToken){
  //console.log(newToken)
  db
  .collection("token")
  .insertOne(newToken,function(err,res){
    if(err)throw err
      console.log("new token inserted")
  });
}

//to be done
function getTransactionsFromAccountId(accountId,accessToken){
  axios({
    method : 'GET',
    url : "https://sandbox.api.macquariebank.io/connect/v1/accounts/"+accountId+"/transactions",
    headers : {
      'client_id' : oauthConfig.clientId,
      'Authorization': accessToken
    }
  }).then(function(response){
    response.status(200)
    return response.data;
  }).catch(function(error){
    console.log(error.response.data)
    return error.response.data;
  });
}


function requestNewAccessToken(refreshToken, res){
  /*
    When requesting for a new access token, we must set the header consisting of client_id and client_secret
    The request will also contain grant_type and refresh token parameters in the body field
    in form of formData
  */

  const {token} = refreshToken
  const refreshData = qs.stringify({grant_type: "refresh_token", refresh_token: refreshToken});

  const refreshConfig = {
    headers: {
      client_id: oauthConfig.clientId
    }

  };
    //send request for a new access token
  axios
    .post(oauthConfig.tokenURL, refreshData, refreshConfig)
    .then(function(response){
        var now = Math.round(new Date().getTime() / 1000);//get the current timestamp
        // retrieve the refresh token, scope, and access token expiry time, as well as refresh token expiry time
        tokenInfo.expiry = now + response.data.expires_in
        tokenInfo.tokenType = response.data.token_type
        tokenInfo.refreshToken = response.data.refresh_token
        tokenInfo.scope = response.data.scope
        tokenInfo.accessToken = response.data.access_token
        tokenInfo.refreshTokenExpiry = now + response.data.refresh_token_expires_in
        tokenInfo.clientId = oauthConfig.clientId

        //delete the old token
        deleteToken()
        //insert the new token into the database
        replaceToken(tokenInfo, oauthConfig.clientId)
        res.status(201).json(response.data);
        //response.status(201).json(response.data)
    })
    .catch(function(error){
      if(error.response){
        res.status(error.response.status).json(error.response.data);
      }
      
    })



}

/*
  checks the validity of current token
  we can assume that there will only be 1 token object for each clientId

*/

doAuth = function(res){
  db
    .collection("token")
    .find({ clientId: oauthConfig.clientId }).toArray(function(err, result){
      if (err) throw err;

      var now = Math.round(new Date().getTime() / 1000);//get the current timestamp

      if(result[0]==null){
        console.log('no token found for this user')
        //redirects the user to Macquarie's login page
        const query = qs.stringify({ client_id: oauthConfig.clientId });
        const url = oauthConfig.authorizationURL;
        res.redirect(`${url}?${query}`);
      }

      else if(result[0].expiry < now){
        console.log("access token expired. requesting for a new token")
        requestNewAccessToken(result[0].refreshToken, res);
      }
      else if(result[0].refreshTokenExpiry < now){
        //if refresh token is expired, the user would need to log in again
        console.log("refresh token expired. Need to ask for new access token");
                      //redirects the user to Macquarie's login page
        const url = oauthConfig.authorizationURL;
        const query = qs.stringify({ client_id: oauthConfig.clientId });

        res.redirect(`${url}?${query}`);

        res.status(301).json({message:'Redirecting'});
      }
      else{
        console.log("token still valid");
        //if the token is still valid, save the token into memory object
        tokenInfo.expiry = result[0].expiry
        tokenInfo.refreshToken = result[0].refreshToken
        tokenInfo.accessToken = result[0].accessToken
        tokenInfo.refreshTokenExpiry = result[0].refreshTokenExpiry
        tokenInfo.clientId = result[0].clientId
        res.status(200).json({message:'token still valid'});
      }
  });
}

/*
  checks the validity of current token
  call this route .../auth/macquarie/ whenever a token check operation is required
  we can assume that there will only be 1 token object for each clientId

*/

router.get("/", (req, res) => {
  doAuth(res);
});

/*
  Get a list of accounts of a user
*/
router.get("/getAccounts",(req,res,next)=>{
  //doAuth(res);
  var accessToken = req.get("Authorization")
  if(accessToken == null){
    res.status(400).json({message:"invalid access token"})
  }
  console.log("accessToken: " + accessToken)
  axios({
    method : 'GET',
    url : "https://sandbox.api.macquariebank.io/connect/v1/accounts",
    headers : {
      'client_id' : oauthConfig.clientId,
      'Authorization':accessToken
    }
  }).then(function(response){
    res.json(response.data);
  }).catch(function(error){
    if(error.response){
      res.status(error.response.status).json(error.response.data);
    }
    
  });
});

/*
  Get list of transactions to be sent to the AI categorisation
*/

router.get("/getTransactions/:accountId",(req,res,next)=>{
  var transactions = [];
  var accountId = req.params.accountId;
  console.log("account Id: "+ accountId);
  var accessToken = req.get("Authorization")
    if(accessToken == null){
    res.status(400).json({message:"invalid access token"})
  }
  /*
    post data to the categorisation module
  */
  axios({
    method : "post",
    url : "http://localhost:8081/api/categorisation",
    data : {
      transactions : getTransactionsFromAccountId(accountId,accessToken)
    },
    headers : {
      'Content-Type' : 'application/json'
    }
  }).then(function(response){
    res.send(response.data)
  });
});

/*
  to revoke user access, simply delete the token from the database
*/
router.get("/revokeToken",(req, res, next)=>{
    deleteToken();
    res.status(200).json({message:'Token deleted'});
});

module.exports = {
  router
};
