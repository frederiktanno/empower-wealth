

const MongoClient = require("mongodb").MongoClient;

const host = require("./config")

const dbName = "auth";
const collection = "token";
var db;
var url = `mongodb://localhost:27017/${dbName}`;//url for connecting to the MongoDB client
		//with the database name of auth

//connects to a MongoDB database, or creates a new one if it does not exist yet

module.exports = {
	connect: function(callback){
		MongoClient.connect(url,function(err,client){
			db = client.db("auth");
			return callback(err);
		})
	},
	getDb: function(){
		return db;
	}
};
